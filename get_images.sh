#/bin/bash

#
# Get images from Ebay
# Author: Hoang Trong Nghia (trongnghiahoang@gmail.com)
# Date: 2018/05/19 
#
# HOW TO USE
# Install packages:
# $ sudo apt-get install libimage-exiftool-perl
#


EBAY_URL=$1
FOLDER=`echo $EBAY_URL | awk -F'/' '{ print $5 }'`
if [ -d "$FOLDER" ]; then
   NEWFOLDER="`date +%s`-$FOLDER"
   mkdir $NEWFOLDER; cd $NEWFOLDER
else
  mkdir -p $FOLDER; cd $FOLDER
fi
for i in `wget -qO- $EBAY_URL | sed -n '/<img/s/.*src="\([^"]*\)".*/\1/p'| grep 's-l64.\|s-l300.\|s-l500.' | sort -u | sed -e "s/s-l64./s-l1000./;s/s-l300./s-l1000./;s/s-l500./s-l1000./"`;
do
   wget -q $i -O `date +%s.%N`;
done
   exiftool -all= *
   rm *_original
cd ..
exit
